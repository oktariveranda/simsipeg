<?php

namespace App\Controllers;

use App\Models\UserModel;
use App\Models\KpiModel;
use App\Models\SettingModel;
use App\Models\ProfileModel;

class Admin extends BaseController
{
    protected $kpi;
    protected $user;
    protected $setting;
    protected $profile;

    public function __construct()
    {
        $this->kpi = new KpiModel();
        $this->user = new UserModel();
        $this->setting = new SettingModel();
        $this->profile = new ProfileModel();
    }

    public function user_setting()
    {
        $users = $this->user->getUsers();
        $profiles = $this->profile->getProfiles();
        $data = [
            'list_users' => $users->getResult('array'),
            'list_profiles' => $profiles->getResult('array'),
            'action' => 'add'
        ];
        $this->site->viewPage('settings/user', true, $data);
    }

    // public function status_setting()
    // {
    //     $user = new UserModel();
    //     $result = $user->getStatusHadir();
    //     $data = ['list_status' => $result->getResult('array')];
    //     $this->site->viewPage('settings/status', true, $data);
    // }

    public function add_user()
    {
        $userId = $this->request->getVar('nik');
        $userName = $this->request->getVar('userName');
        $password = $this->site->encodePassword(PASSWORD_SALT, $userId);
        $profile = $this->request->getVar('profile');
        $createdBy = session()->get('user_id');

        if($profile == '0')
        {
            session()->setFlashdata('error', 'Pilih profil pegawai baru!');
            return redirect()->back()->withInput();
        }

        $checkUser = $this->user->authenticateUser($userId, '');
        if($this->site->validateQueryResult($checkUser))
        {
            session()->setFlashdata('error', 'User dengan NIK <b>'.$userId.'</b> sudah ada!');
            return redirect()->back();
        }

        $result = $this->user->addUser($userId, $userName, $password, $profile, $createdBy);
        if($this->site->validateQueryResult($result))
        {
            session()->setFlashdata('info', 'Berhasil tambah user!');
            return redirect()->back();
        }
        else
        {
            session()->setFlashdata('error', 'Gagal tambah user!');
            return redirect()->back()->withInput();
        }
    }

    public function edit_user($userId = '')
    {
        if($userId != '')
        {
            $userDetail = $this->user->getUserDetail($userId);
            if($this->site->validateQueryResult($userDetail))
            {
                $users = $this->user->getUsers();
                $profiles = $this->profile->getProfiles();
                $data = [
                    'list_users' => $users->getResult('array'),
                    'list_profiles' => $profiles->getResult('array'),
                    'user_detail' => $userDetail->getRowArray(),
                    'action' => 'edit'
                ];
                $this->site->viewPage('settings/user', true, $data);
            }
        }
    }

    public function update_user()
    {
        $userId = $this->request->getVar('hiddenNik');
        $userName = $this->request->getVar('userName');
        $profile = $this->request->getVar('profile');
        $isDeleted = $this->request->getVar('isDeleted');
        $updatedBy = session()->get('user_id');
        $result = $this->user->updateUser($userId, '', '', 'N', $userName, $profile, $isDeleted, $updatedBy);
        if($this->site->validateQueryResult($result))
        {
            session()->setFlashdata('info', 'Data pegawai <b>'.$userId.'</b> berhasil diubah!');
            return redirect()->to('/user_setting');
        }
    }

    public function kpi()
    {
        $result = $this->kpi->getKPI();
        $data = ['list_kpi' => array(), 'action' => 'add', 'addFlag' => ($result->getNumRows() < KPI_LIMIT) ? false:true];
        if($this->site->validateQueryResult($result))
        {
            $data['list_kpi'] = $result->getResult('array');
        }
        $this->site->viewPage('kpi/index', true, $data);
    }

    public function kpi_add()
    {
        $kpiName = $this->request->getVar('kpiName');
        $createdBy = session()->get('user_id');
        $addKPI = $this->kpi->addKPI($kpiName, $createdBy);
        if($this->site->validateQueryResult($addKPI))
        {
            $totalKPI = $addKPI->getRowArray()['Total_KPI'];
            session()->setFlashdata('info', 'Indikator <b>'.$kpiName.'</b> berhasil ditambah! Total KPI: '.$totalKPI.' dari '.KPI_LIMIT.' yang ada.');
            return redirect()->to('/kpi');
        }
    }

    public function edit_kpi($kpiId = '')
    {
        if($kpiId != '')
        {
            $kpiDetail = $this->kpi->getKPIDetail($kpiId);
            if($this->site->validateQueryResult($kpiDetail))
            {
                $kpiList = $this->kpi->getKPI();
                $data = [
                    'list_kpi' => $kpiList->getResult('array'), 
                    'kpi_detail' => $kpiDetail->getRowArray(),
                    'action' => 'edit',
                    'addFlag' => ($kpiDetail->getNumRows() < KPI_LIMIT) ? false:true
                ];
                $this->site->viewPage('kpi/index', true, $data);   
            }
        }
    }

    public function update_KPI()
    {
        $kpiId = $this->request->getVar('hiddenKpiId');
        $kpiName = $this->request->getVar('kpiName');
        $updatedBy = session()->get('user_id');
        $result = $this->kpi->updateKPI($kpiId, $kpiName, $updatedBy);
        if($this->site->validateQueryResult($result))
        {
            session()->setFlashdata('info', 'Indikator <b>'.$kpiName.'</b> berhasil diubah!');
            return redirect()->to('/kpi');
        }
    }

    public function kpi_history()
    {
        $this->site->viewPage('kpi/history');
    }

    public function working_setting()
    {
        $awal = $this->setting->getSettingDetail(1);
        $akhir = $this->setting->getSettingDetail(2);
        $data = [
            'awal' => $awal->getRowArray(),
            'akhir' => $akhir->getRowArray()
        ];
        $this->site->viewPage('settings/working_hour', true, $data);
    }

    public function define_working_setting()
    {
        $settingId = $this->request->getVar('hiddenSettingId');
        $settingId_2 = $this->request->getVar('hiddenSettingId_2');
        $settingValue = $this->request->getVar('jamOperasionalAwal');
        $settingValue_2 = $this->request->getVar('jamOperasionalAkhir');

        if($settingValue_2 < $settingValue)
        {
            session()->setFlashdata('error', 'Jam Operasional gagal diubah karena Jam akhir lebih kecil dari Jam awal!');
            return redirect()->back()->withInput();
        }
        else
        {
            $createdBy = session()->get('user_id');
            $result = $this->setting->updateSetting($settingId, $settingValue, $createdBy);
            $result_2 = $this->setting->updateSetting($settingId_2, $settingValue_2, $createdBy);
            if($this->site->validateQueryResult($result) && $this->site->validateQueryResult($result_2))
            {
                session()->setFlashdata('info', 'Jam Operasional berhasil diubah!');
                return redirect()->to('/working_setting');
            }
            else
            {
                session()->setFlashdata('error', 'Jam Operasional gagal diubah!');
                return redirect()->to('/working_setting');
            }
        }
    }

}