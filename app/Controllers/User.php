<?php

namespace App\Controllers;

use App\Models\UserModel;

class User extends BaseController
{

    protected $user;

    public function __construct()
    {
        if((is_null(session()->get('logged_in')) && (session()->get('logged_in') != '')))
        {
            return redirect()->to('/login');
        }
        $this->user = new UserModel();
    }

    public function index()
    {
        $this->site->viewPage('user/index');
    }

    public function profile()
    {
        $this->site->viewPage('user/profile');
    }

    public function change_password()
    {
        $userId = $this->request->getVar('hiddenNik');
        $oldPassword = $this->request->getVar('password_lama');
        $newPassword = $this->request->getVar('password_baru');
        $confirmPassword = $this->request->getVar('confirm_password_baru');
        if($newPassword != $confirmPassword)
        {
            session()->setFlashdata('error', 'Konfirmasi password salah!');
            return redirect()->back()->withInput();
        }
        else
        {
            $result = $this->user->authenticateUser($userId, $this->site->encodePassword(PASSWORD_SALT, $oldPassword));
            if($this->site->validateQueryResult($result))
            {
                $result = $this->user->updateUser(
                    $userId, $this->site->encodePassword(PASSWORD_SALT, $oldPassword),
                    $this->site->encodePassword(PASSWORD_SALT, $newPassword),'N','','','N',session()->get('user_id')
                );
                if($this->site->validateQueryResult($result))
                {
                    session()->setFlashdata('info', 'Password berhasil diubah!');
                    return redirect()->back()->withInput();
                }
                else
                {
                    session()->setFlashdata('error', 'Password lama salah, silakan periksa kembali!');
                    return redirect()->back()->withInput();    
                }
            }
            else
            {
                session()->setFlashdata('error', 'Password lama salah, silakan periksa kembali!');
                return redirect()->back()->withInput();
            }
        }
    }

}