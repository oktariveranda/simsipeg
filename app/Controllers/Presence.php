<?php

namespace App\Controllers;

use App\Models\PresenceModel;
use App\Models\UserModel;
use App\Models\ProfileModel;
use App\Models\StatusModel;
use App\Models\SettingModel;

class Presence extends BaseController
{

    protected $user;
    protected $profile;
    protected $status;
    protected $presence;
    protected $setting;

    public function __construct()
    {
        if((is_null(session()->get('logged_in')) && (session()->get('logged_in') != '')))
        {
            return redirect()->to('/login');
        }
        $this->user = new UserModel();
        $this->profile = new ProfileModel();
        $this->status = new StatusModel();
        $this->presence = new PresenceModel();
        $this->setting = new SettingModel();
    }

    public function history()
    {
        $userId = session()->get('user_id');
        $profile = session()->get('profile');
        if($profile == PROFILE_EMPLOYEE)
        {
            $userHistory = $this->presence->getUserHistoricalPresence($userId);
            $overtime = $this->presence->getOvertime($userId);
        }
        else
        {
            $userHistory = $this->presence->getUserHistoricalPresence();
            $overtime = $this->presence->getOvertime();
        }

        $awal = $this->setting->getSettingDetail(1);
        $akhir = $this->setting->getSettingDetail(2);
        $data = [
            'list_presence' => ($this->site->validateQueryResult($userHistory)) ? $userHistory->getResult('array') : null,
            'list_overtime' => ($this->site->validateQueryResult($overtime)) ? $overtime->getResult('array') : null,
            'awal' => $awal->getRowArray(),
            'akhir' => $akhir->getRowArray()
        ];
        $this->site->viewPage('user/presence_history', true, $data);
    }

    public function input($userId = '')
    {
        if($userId == '')
        {
            $users = $this->presence->getUsersPresence();
            $profiles = $this->profile->getProfiles();
            $data = [
                'list_users' => $users->getResult('array'),
                'list_profiles' => $profiles->getResult('array'),
                'action' => 'view'
            ];
            $this->site->viewPage('presence/manual', true, $data);
        }
        else
        {
            $users = $this->presence->getUsersPresence();
            $profiles = $this->profile->getProfiles();
            $userDetail = $this->presence->getUserDetailPresence($userId);
            $presenceStatusList = $this->status->getStatusHadir();
            $data = [
                'list_users' => $users->getResult('array'),
                'list_profiles' => $profiles->getResult('array'),
                'action' => 'add',
                'user_detail' => $userDetail->getRowArray(),
                'list_status' => $presenceStatusList->getResult('array')
            ];
            $this->site->viewPage('presence/manual', true, $data);
        }
    }

    public function process()
    {
        $userId = $this->request->getVar('hiddenNik');
        $presenceStatus = $this->request->getVar('presenceStatus');
        $updatedBy = session()->get('user_id');

        if($presenceStatus == '0')
        {
            session()->setFlashdata('error', 'Pilih status kehadiran!');
            return redirect()->back()->withInput();
        }

        $updatePresence = $this->presence->updatePresence($userId, $updatedBy, $presenceStatus);
        if($this->site->validateQueryResult($updatePresence))
        {
            if(STATUS_ABSEN == $presenceStatus)
            {
                $totalAbsence = $this->presence->getTotalAbsence($userId);
                if($this->site->validateQueryResult($totalAbsence))
                {
                    $totalAbsence = $totalAbsence->getRow()->total_record;
                }

                if(!is_null($totalAbsence) && $totalAbsence >= ABSENCE_LIMIT)
                {
                    $result = $this->user->updateUser($userId, null, null, 'N', null, null, 'Y',session()->get('user_id'));
                    if($this->site->validateQueryResult($result))
                    {
                        session()->setFlashdata('error', 'Pegawai dengan NIK '.$userId.' telah absen sebanyak '.$totalAbsence.' kali. Pegawai telah dinonaktifkan!');
                    }
                }   
            }
            session()->setFlashdata('info', 'Berhasil melakukan perubahan status kehadiran!');
            return redirect()->to('/manual_input');
        }
        else
        {
            session()->setFlashdata('error', 'Gagal melakukan perubahan status kehadiran!');
            return redirect()->to('/manual_input');
        }
    }

    public function presence_form()
    {
        $this->site->viewPage('user/presence', false);
    }

    public function presence_process()
    {
        $userId = $this->request->getVar('nik');
        $result = $this->user->authenticateUser($userId, NULL);
        $presenceStatus = STATUS_HADIR;
        $updatedBy = 'System';
        $infoMessage = 'Pegawai <b>'.$userId.'</b> berhasil tap in!';
        if ($this->site->validateQueryResult($result)) 
        {
            $checkTodayPresence = $this->presence->checkTodayPresence($userId);
            if($this->site->validateQueryResult($checkTodayPresence))
            {
                if($checkTodayPresence->getRowArray()['total_presence'] > 1)
                {
                    session()->setFlashdata('error', 'Gagal input presensi pegawai dengan NIK <b>'.$userId.'</b> karena sudah melakukan presensi!');
                    return redirect()->back();
                }
                else
                {
                    if($checkTodayPresence->getRowArray()['total_presence'] == 1)
                    {
                        $presenceStatus = STATUS_PULANG;
                        $infoMessage = 'Pegawai <b>'.$userId.'</b> berhasil tap out!';
                    }
                }
            }

            if($presenceStatus == STATUS_HADIR)
            {
                $isLate = $this->setting->getSettingDetail(1);
                $workingHour_Hour = date("H", strtotime($isLate->getRowArray()['setting_value']));;
                $workingHour_Minute = date("i", strtotime($isLate->getRowArray()['setting_value']));;
                $workingHour_Second = date("s", strtotime($isLate->getRowArray()['setting_value']));;

                $endTime = $this->site->addMinutesToTime($workingHour_Hour, $workingHour_Minute + 15, $workingHour_Second);
                
                if(date("Y-m-d H:i:s") > $endTime)
                {
                    $presenceStatus = STATUS_TERLAMBAT;
                }
            }
            
            $updatePresence = $this->presence->updatePresence($userId, $updatedBy, $presenceStatus);
            if($this->site->validateQueryResult($updatePresence))
            {

                $totalTerlambat = $this->presence->getTotalKeterlambatan($userId);
                if($this->site->validateQueryResult($totalTerlambat))
                {
                    $totalTerlambat = $totalTerlambat->getRow()->total_record;
                }

                session()->setFlashdata('info', $infoMessage);
                if(!is_null($totalTerlambat) && $totalTerlambat >= TARDINESS_LIMIT && $totalTerlambat != '')
                {
                    session()->setFlashdata('error', 'Pegawai dengan NIK '.$userId.' mendapatkan SP karena telah terlambat sebanyak '.$totalTerlambat.' kali!');
                }

                return redirect()->back();
            }
            else
            {
                session()->setFlashdata('error', 'Gagal input presensi pegawai dengan NIK <b>'.$userId.'</b>!');
                return redirect()->back();
            }

        }
        else
        {
            session()->setFlashdata('error', 'Data pegawai <b>'.$userId.'</b> tidak ditemukan atau pegawai telah resign/tidak aktif!');
            return redirect()->back();
        }
    }

}
