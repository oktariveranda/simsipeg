<?php

namespace App\Controllers;

use App\Models\PresenceModel;
use App\Models\UserModel;

class Authentication extends BaseController
{

    protected $user;
    protected $presence;

    public function __construct()
    {
        $this->user = new UserModel();
        $this->presence = new PresenceModel();
    }

    public function index()
    {
        $this->site->viewPage('user/login', false);
    }

    public function login()
    {
        $userId = $this->request->getVar('nik');
        $password = $this->request->getVar('password');
        $result = $this->user->authenticateUser($userId, $this->site->encodePassword(PASSWORD_SALT, $password));
        
        if ($this->site->validateQueryResult($result)) {
            $totalTerlambat = 0;
            if(PROFILE_EMPLOYEE == $result->getRow()->profile)
            {
                $totalTerlambat = $this->presence->getTotalKeterlambatan($userId);
                if($this->site->validateQueryResult($totalTerlambat))
                {
                    $totalTerlambat = $totalTerlambat->getRow()->total_record;
                }
            }
            session()->set([
                'user_id' => $userId,
                'profile' => $result->getRow()->profile,
                'last_login' => $result->getRow()->last_login, 
                'user_name' => $result->getRow()->user_name, 
                'logged_in' => TRUE
            ]);
            if(is_null(session()->get('last_login')))
            {
                return redirect()->to(base_url('/profile'));
            }
            else
            {
                if(!is_null($totalTerlambat) && $totalTerlambat >= TARDINESS_LIMIT && $totalTerlambat != '')
                {
                    session()->setFlashdata('error', 'Anda mendapatkan SP karena telah terlambat sebanyak '.$totalTerlambat.' kali!');
                }
                return redirect()->to(base_url('/'));
            }
        } 
        else 
        {
            session()->setFlashdata('error', 'Username atau Password Salah');
            return redirect()->back()->withInput();
        }
    }

    public function logout()
    {
        $userId = session()->get('user_id');
        $result = $this->user->updateUser($userId, '', '', 'Y', '', '', 'N', session()->get('user_id'));
        if($this->site->validateQueryResult($result))
        {
            session()->destroy();
            return redirect()->to('/');
        }
        return;
    }

}