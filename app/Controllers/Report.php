<?php

namespace App\Controllers;

use App\Models\PresenceModel;
use App\Models\UserModel;

class Report extends BaseController
{

    protected $user;
    protected $presence;

    public function __construct()
    {
        if((is_null(session()->get('logged_in')) && (session()->get('logged_in') != '')))
        {
            return redirect()->to('/login');
        }
        $this->user = new UserModel();
        $this->presence = new PresenceModel();
    }

    public function kehadiran()
    {
        $employees = $this->user->getEmployees();
        $data = [
            'employees' => ($this->site->validateQueryResult($employees)) ? $employees->getResult('array') : null,
            'months' => $this->site->getMonths(),
            'years' => $this->site->getYears(),
            'report' => null,
            'isProcessed' => false
        ];
        $this->site->viewPage('report/kehadiran', true, $data);
    }

    public function kehadiran_process()
    {
        $userId = $this->request->getVar('employee_id');
        $month = $this->request->getVar('month');
        $year = $this->request->getVar('year');
        $result = $this->presence->getPresenceReport($userId, $month, $year);
        $employees = $this->user->getEmployees();
        $userDetail = $this->user->getUserDetail($userId);
        $data = [
            'report' => ($this->site->validateQueryResult($result)) ? $result->getResult('array') : null,
            'employees' => ($this->site->validateQueryResult($employees)) ? $employees->getResult('array') : null,
            'months' => $this->site->getMonths(),
            'years' => $this->site->getYears(),
            'selected_month' => $month,
            'selected_year' => $year,
            'userDetail' => ($this->site->validateQueryResult($userDetail)) ? $userDetail->getResult('array') : null,
            'isProcessed' => true
        ];
        $this->site->viewPage('report/kehadiran', true, $data);
    }

    public function lembur()
    {
        $data = [
            'months' => $this->site->getMonths(),
            'years' => $this->site->getYears(),
            'report' => null,
            'isProcessed' => false
        ];
        $this->site->viewPage('report/lembur', true, $data);
    }

    public function lembur_process()
    {
        $month = $this->request->getVar('month');
        $year = $this->request->getVar('year');
        $result = $this->presence->getOvertimeReport(null, $month, $year);
        $data = [
            'report' => ($this->site->validateQueryResult($result)) ? $result->getResult('array') : null,
            'months' => $this->site->getMonths(),
            'years' => $this->site->getYears(),
            'selected_month' => $month,
            'selected_year' => $year,
            'isProcessed' => true
        ];
        $this->site->viewPage('report/lembur', true, $data);
    }

}