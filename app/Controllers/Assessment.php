<?php

namespace App\Controllers;

use App\Models\KpiModel;
use App\Models\AssessmentModel;
use App\Models\UserModel;
use PhpParser\Node\Stmt\Else_;

class Assessment extends BaseController
{

    protected $kpi;
    protected $assessment;
    protected $user;

    public function __construct()
    {
        $this->kpi = new KpiModel();
        $this->assessment = new AssessmentModel();
        $this->user = new UserModel();
    }

    public function index()
    {
        $users = $this->assessment->getKPIPegawai();
        $data = [
            'list_users' => $users->getResult('array'),
            'action' => 'index'
        ];
        $this->site->viewPage('assessment/index', true, $data);
    }

    public function input_kpi($userId)
    {
        $kpiList = $this->kpi->getKPI();
        $users = $this->assessment->getKPIPegawai();
        $userDetail = $this->user->getUserDetail($userId)->getRowArray();
        $data = [
            'list_kpi' => $kpiList->getResult('array'),
            'list_users' => $users->getResult('array'),
            'action' => 'input_kpi',
            'user_id' => $userId,
            'user_name' => ($userDetail != null) ? $userDetail['user_name'] : ''
        ];
        $this->site->viewPage('assessment/index', true, $data);
    }

    public function view_kpi($userId, $year = '')
    {
        $users = $this->assessment->getKPIPegawai();
        $assessmentList = $this->assessment->getKPIPegawaiDetail($userId, $year);
        $userDetail = $this->user->getUserDetail($userId)->getRowArray();
        $data = [
            'assessment_list' => $assessmentList->getResult('array'),
            'list_users' => $users->getResult('array'),
            'action' => 'view_kpi',
            'user_id' => $userId,
            'user_name' => ($userDetail != null) ? $userDetail['user_name'] : '',
            'year' => ($year == '') ? date('Y') : $year
        ];
        $this->site->viewPage('assessment/index', true, $data);
    }

    public function input_kpi_pegawai()
    {
        $kpiList = $this->kpi->getKPI();
        $userId = $this->request->getVar('hiddenUserId');
        $year = $this->request->getVar('hiddenYear');
        $createdBy = session()->get('user_id');
        $counter = 0;

        $checkAssessment = $this->assessment->getKPIPegawaiDetail($userId, $year);
        if($this->site->validateQueryResult($checkAssessment))
        {
            session()->setFlashdata('error', 'Sudah dilakukan assessment untuk pegawai ini!');
        }
        else
        {
            if(count($kpiList->getResult('array')) == KPI_LIMIT)
            {
                foreach($kpiList->getResult('array') as $kpiRow)
                {
                    if($this->site->validateQueryResult($this->assessment->addKPIPegawai(
                        $this->request->getVar('hiddenKPIId_'.$kpiRow['kpi_id']),
                        $this->request->getVar('hiddenKPIName_'.$kpiRow['kpi_id']),
                        $userId,
                        $createdBy,
                        $this->request->getVar('score_'.$kpiRow['kpi_id']),
                        $year
                    )))
                    {
                        $counter++;
                    }
                }
                session()->setFlashdata('info', 'Selesai input KPI!');
            }
            else
            {
                session()->setFlashdata('error', 'Gagal input KPI karena total KPI tidak sama dengan komponen KPI yang sudah diset yaitu sebesar '.KPI_LIMIT.'!');
            }
            
        }
        return redirect()->to('/input_kpi');
    }

}