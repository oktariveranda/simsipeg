<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->add('/', 'Home::index');
$routes->get('/main', 'Presence::presence_form');
$routes->post('/main', 'Presence::presence_process');
$routes->add('/login', 'Authentication::index');
$routes->add('/login_auth', 'Authentication::login');
$routes->add('/logout', 'Authentication::logout');
$routes->add('/profile', 'User::profile');
$routes->add('/change_password', 'User::change_password');
$routes->get('/user_setting', 'Admin::user_setting');
$routes->get('/working_setting', 'Admin::working_setting');
$routes->post('/working_setting', 'Admin::define_working_setting');
// $routes->get('/status_setting', 'Admin::status_setting');
$routes->post('/add_user', 'Admin::add_user');
$routes->get('/edit_user/(:any)', 'Admin::edit_user/$1');
$routes->post('/update_user', 'Admin::update_user');
$routes->get('/kpi', 'Admin::kpi');
$routes->post('/add_kpi', 'Admin::kpi_add');
$routes->get('/edit_kpi/(:any)', 'Admin::edit_kpi/$1');
$routes->post('/update_kpi', 'Admin::update_kpi');
$routes->get('/input_kpi', 'Assessment::index');
$routes->get('/input_kpi/(:any)', 'Assessment::input_kpi/$1');
$routes->get('/view_kpi/(:any)/(:num)', 'Assessment::view_kpi/$1/$2');
$routes->post('/input_kpi', 'Assessment::input_kpi_pegawai');
$routes->get('/presence_history', 'Presence::history');
$routes->get('/manual_input', 'Presence::input');
$routes->get('/manual_input/(:any)', 'Presence::input/$1');
$routes->post('/manual_input', 'Presence::process');
$routes->get('/kpi_history', 'Admin::kpi_history');

$routes->get('/report/kehadiran', 'Report::kehadiran');
$routes->post('/report/kehadiran', 'Report::kehadiran_process');
$routes->get('/report/lembur', 'Report::lembur');
$routes->post('/report/lembur', 'Report::lembur_process');


/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}