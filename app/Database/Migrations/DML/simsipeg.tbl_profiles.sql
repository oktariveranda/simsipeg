INSERT INTO simsipeg.tbl_profiles (profile_id,profile_name,created_by,created_date,updated_by,updated_date,is_deleted) VALUES
	 ('A','Administrator','System','2021-10-25 00:59:53',NULL,NULL,'N');
INSERT INTO simsipeg.tbl_profiles (profile_id,profile_name,created_by,created_date,updated_by,updated_date,is_deleted) VALUES
	 ('O','Owner','System', sysdate(),NULL,NULL,'N');
INSERT INTO simsipeg.tbl_profiles (profile_id,profile_name,created_by,created_date,updated_by,updated_date,is_deleted) VALUES
	 ('M','Manager','System', sysdate(),NULL,NULL,'N');
INSERT INTO simsipeg.tbl_profiles (profile_id,profile_name,created_by,created_date,updated_by,updated_date,is_deleted) VALUES
	 ('E','Employee','System', sysdate(),NULL,NULL,'N');