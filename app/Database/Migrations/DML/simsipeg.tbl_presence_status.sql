INSERT INTO simsipeg.tbl_presence_status (status_code, status_desc, created_by, created_date, updated_by, updated_date) 
VALUES('H', 'Hadir', 'System', sysdate(), NULL, NULL);
INSERT INTO simsipeg.tbl_presence_status (status_code, status_desc, created_by, created_date, updated_by, updated_date) 
VALUES('I', 'Izin/Cuti', 'System', sysdate(), NULL, NULL);
INSERT INTO simsipeg.tbl_presence_status (status_code, status_desc, created_by, created_date, updated_by, updated_date) 
VALUES('A', 'Absen', 'System', sysdate(), NULL, NULL);
INSERT INTO simsipeg.tbl_presence_status (status_code, status_desc, created_by, created_date, updated_by, updated_date) 
VALUES('T', 'Terlambat', 'System', sysdate(), NULL, NULL);
INSERT INTO simsipeg.tbl_presence_status (status_code, status_desc, created_by, created_date, updated_by, updated_date) 
VALUES('TI', 'Terlambat dengan izin', 'System', sysdate(), NULL, NULL);
INSERT INTO simsipeg.tbl_presence_status (status_code, status_desc, created_by, created_date, updated_by, updated_date) 
VALUES('P', 'Pulang', 'System', sysdate(), NULL, NULL);