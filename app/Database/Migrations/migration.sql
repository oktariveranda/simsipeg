CREATE DATABASE `simsipeg` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
source Tables/simsipeg.tbl_profiles.sql
source Tables/simsipeg.tbl_presence_status.sql
source Tables/simsipeg.tbl_users.sql
source Tables/simsipeg.tbl_kpi.sql
source Tables/simsipeg.tbl_presence.sql
source Tables/simsipeg.tbl_assessments.sql
source Tables/simsipeg.tbl_settings.sql

source DML/[all].sql
source/Programmability/[all].sql