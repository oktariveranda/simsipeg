-- simsipeg.tbl_presence definition

CREATE TABLE `tbl_presence` (
  `presence_id` bigint NOT NULL AUTO_INCREMENT,
  `presence_date` datetime NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `status` char(3) NOT NULL,
  PRIMARY KEY (`presence_id`),
  KEY `tbl_presence_FK` (`user_id`),
  KEY `tbl_presence_presence_id_IDX` (`presence_id`,`user_id`) USING BTREE,
  KEY `tbl_presence_FK_1` (`status`),
  CONSTRAINT `tbl_presence_FK` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;