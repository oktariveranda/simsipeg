-- simsipeg.tbl_users definition

CREATE TABLE `tbl_users` (
  `user_id` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(100) NOT NULL,
  `profile` char(1) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT 'System',
  `created_date` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_deleted` char(1) NOT NULL DEFAULT 'N',
  `session_locked` char(1) NOT NULL DEFAULT 'N',
  `user_name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  KEY `tbl_users_FK` (`profile`),
  CONSTRAINT `tbl_users_FK` FOREIGN KEY (`profile`) REFERENCES `tbl_profiles` (`profile_id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Menyimpan data pengguna';