-- simsipeg.tbl_assessments definition
CREATE TABLE `tbl_assessments` (
  `assessment_id` bigint NOT NULL AUTO_INCREMENT,
  `kpi_id` int NOT NULL,
  `kpi_name` varchar(500) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `score` int NOT NULL,
  `year` int NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`assessment_id`),
  UNIQUE KEY `tbl_assessments_assessment_id_IDX` (`assessment_id`) USING BTREE,
  KEY `tbl_assessments_kpi_id_IDX` (`kpi_id`,`user_id`) USING BTREE,
  CONSTRAINT `tbl_assessments_FK` FOREIGN KEY (`kpi_id`) REFERENCES `tbl_kpi` (`kpi_id`),
  CONSTRAINT `tbl_assessments_FK_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;