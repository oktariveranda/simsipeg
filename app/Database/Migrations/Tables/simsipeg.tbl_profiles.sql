-- simsipeg.tbl_profiles definition

CREATE TABLE `tbl_profiles` (
  `profile_id` char(1) NOT NULL,
  `profile_name` varchar(100) NOT NULL,
  `created_by` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT 'System',
  `created_date` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `is_deleted` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`profile_id`),
  UNIQUE KEY `tbl_profiles_profile_id_IDX` (`profile_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;