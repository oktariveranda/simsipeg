-- simsipeg.tbl_kpi definition

CREATE TABLE `tbl_kpi` (
  `kpi_id` int NOT NULL AUTO_INCREMENT,
  `kpi_name` varchar(500) NOT NULL,
  `created_by` varchar(100) DEFAULT 'System',
  `created_date` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`kpi_id`),
  UNIQUE KEY `tbl_kpi_kpi_id_IDX` (`kpi_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;