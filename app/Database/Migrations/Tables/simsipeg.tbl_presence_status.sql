-- simsipeg.tbl_presence_status definition

CREATE TABLE `tbl_presence_status` (
  `status_code` char(3) NOT NULL,
  `status_desc` varchar(100) NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`status_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;