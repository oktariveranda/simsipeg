DROP PROCEDURE simsipeg.sp_AddUser;
CREATE PROCEDURE simsipeg.sp_AddUser(
    IN user_id_in NVARCHAR(100), 
    IN user_name_in NVARCHAR(100),
    IN password_in NVARCHAR(100),
    IN profile_in CHAR(1),
    IN created_by_in NVARCHAR(100)
)
BEGIN
    
    INSERT INTO tbl_users (user_id, password, profile, created_by, created_date, user_name)
    VALUES (user_id_in, password_in, profile_in, created_by_in, sysdate(), user_name_in);

    SELECT 'Success' Status FROM dual;
END;