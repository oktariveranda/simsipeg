DROP PROCEDURE simsipeg.sp_GetOvertime;
CREATE PROCEDURE simsipeg.sp_GetOvertime(
    IN user_id_in NVARCHAR(100)
)
BEGIN
    select 
        res.presence_time_tap_out, res.overtime_work, res.presence_date_tap_out, res.user_id, res.user_name, res.profile_name profile,
        SUBTIME(res.presence_time_tap_out, res.overtime_work) time_difference
    from (
        select
            DATE_FORMAT(tp.presence_date,'%H:%i:%s') presence_time_tap_out,
            DATE_FORMAT(tp.presence_date, "%d %M %Y") presence_date_tap_out,
            tu.user_id,
            tu.user_name,
            (
                select 
                    date_add(convert(setting_value, time), interval 00 minute)
                from tbl_settings ts where setting_id = 2
            ) overtime_work,            
            tp.status,
            tpr.profile_name
        from tbl_presence tp
        inner join tbl_users tu on tu.user_id = tp.user_id
        left join tbl_profiles tpr on tpr.profile_id = tu.profile 
        where tp.status = 'P' and tp.user_id = (case when (user_id_in is not null and user_id_in <> '') then user_id_in else tp.user_id end)
    ) res
    where res.presence_time_tap_out > res.overtime_work;
END;