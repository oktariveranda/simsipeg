DROP PROCEDURE simsipeg.sp_GetKPIPegawaiDetail;
CREATE PROCEDURE simsipeg.sp_GetKPIPegawaiDetail(
    IN user_id_in NVARCHAR(100),
    IN year_in INT
)
BEGIN
    SELECT 
        ta.assessment_id ,
        tk.kpi_name ,
        ta.score ,
        ta.`year` ,
        ta.user_id 
    FROM tbl_users tu 
    inner join tbl_assessments ta on tu.user_id = ta.user_id 
    inner join tbl_kpi tk on tk.kpi_id = ta.kpi_id 
    where ta.user_id = user_id_in and ta.`year` = year_in;
END;