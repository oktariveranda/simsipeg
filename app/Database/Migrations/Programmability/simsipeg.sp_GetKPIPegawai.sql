DROP PROCEDURE simsipeg.sp_GetKPIPegawai;
CREATE PROCEDURE simsipeg.sp_GetKPIPegawai()
BEGIN
    select 
        max(tu.user_id) user_id,
        max(tu.user_name) user_name,
        (
            case when (max(tu.is_deleted) = 'N') 
            then 'Aktif'
            else 'Tidak Aktif'
            end
        ) is_deleted,
        round(avg(ta.score), 1) score,
        max(ta.`year`) `year`,
        (select count(kpi_id) from tbl_kpi) total_kpi
    from tbl_users tu 
    left join tbl_assessments ta on ta.user_id = tu.user_id 
    left join tbl_kpi tk on ta.kpi_id = tk.kpi_id
    where tu.profile = 'E'
    group by tu.user_id , ta.`year` 
    order by ta.`year` desc;
END;