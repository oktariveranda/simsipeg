DROP PROCEDURE simsipeg.sp_GetUsersPresence;
CREATE PROCEDURE simsipeg.sp_GetUsersPresence(
    IN profile_in CHAR(1)
)
BEGIN
    select
        tu.user_id user_id,
        tu.user_name user_name,
        tpr.status status_code,
        tu.is_deleted is_deleted,
        filtered.last_presence_date last_presence_date,
        tps.status_desc status_desc,
        tp.profile_name profile
    from tbl_users tu 
    left join tbl_profiles tp on tp.profile_id = tu.profile 
    left join tbl_presence tpr on tpr.user_id = tu.user_id
    left join tbl_presence_status tps on tps.status_code = tpr.status
    inner join (select max(presence_date) last_presence_date, user_id from tbl_presence group by user_id) filtered on filtered.user_id = tpr.user_id
    where tu.profile = CASE WHEN (profile_in IS NOT NULL AND profile_in <> '') THEN profile_in ELSE tu.profile END
    and tpr.presence_date = filtered.last_presence_date
    order by tu.updated_date desc;
END;