DROP PROCEDURE simsipeg.sp_GetUserDetailPresence;
CREATE PROCEDURE simsipeg.sp_GetUserDetailPresence(
    IN user_id_in NVARCHAR(100)
)
BEGIN
    select
        tu.user_id user_id,
        tu.user_name user_name,
        tpr.status recent_status,
        tu.is_deleted is_deleted,
        tpr.presence_date last_presence_date,
        tu.profile profile
    from tbl_users tu 
    inner join tbl_profiles tp on tp.profile_id = tu.profile 
    left join tbl_presence tpr on tpr.user_id = tu.user_id
    where tu.user_id = user_id_in
    order by tu.updated_date desc
    limit 1;
END;