DROP PROCEDURE simsipeg.sp_GetProfiles;
CREATE PROCEDURE simsipeg.sp_GetProfiles()
BEGIN
    SELECT * FROM tbl_profiles tp ORDER BY tp.updated_date DESC;
END;