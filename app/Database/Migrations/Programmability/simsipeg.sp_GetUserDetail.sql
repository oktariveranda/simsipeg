DROP PROCEDURE simsipeg.sp_GetUserDetail;
CREATE PROCEDURE simsipeg.sp_GetUserDetail(
    IN user_id_in NVARCHAR(100)
)
BEGIN
    SELECT * FROM tbl_users tu where tu.user_id = user_id_in;
END;