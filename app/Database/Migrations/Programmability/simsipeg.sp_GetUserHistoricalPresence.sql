DROP PROCEDURE simsipeg.sp_GetUserHistoricalPresence;
CREATE PROCEDURE simsipeg.sp_GetUserHistoricalPresence(
    IN user_id_in NVARCHAR(100)
)
BEGIN

    SELECT 
        Hadir.user_id, 
        Hadir.user_name, 
        Hadir.presence_date as hadir_date, 
        Pulang.presence_date as pulang_date, 
        Hadir.status as hadir_status, 
        Hadir.status_desc as hadir_status_desc, 
        Pulang.status as pulang_status, 
        Pulang.status_desc as pulang_status_desc, 
        Hadir.profile
    FROM
    (select
            tu.user_id,
            tu.user_name,
            tpr.presence_date,
            tpr.status,
            tps.status_desc,
            tp.profile_name as profile
        from tbl_presence tpr
        inner join tbl_users tu on tu.user_id = tpr.user_id
        left join tbl_presence_status tps on tps.status_code = tpr.status
        left join tbl_profiles tp on tu.profile = tp.profile_id
        where tpr.status <> 'P'
        ) as Hadir
    LEFT JOIN
    (
    select
            tu.user_id,
            tu.user_name,
            tpr.presence_date,
            tpr.status,
            tps.status_desc,
            tp.profile_name as profile
        from tbl_presence tpr
        inner join tbl_users tu on tu.user_id = tpr.user_id
        left join tbl_presence_status tps on tps.status_code = tpr.status
        left join tbl_profiles tp on tu.profile = tp.profile_id
        where tpr.status = 'P'
    ) as Pulang ON Pulang.user_id = Hadir.user_id AND date(Hadir.presence_date) = date(Pulang.presence_date)
    WHERE Hadir.user_id = (case when (user_id_in is not null and user_id_in <> '') then user_id_in else Hadir.user_id end)
    ORDER BY Hadir.presence_date desc;
END;