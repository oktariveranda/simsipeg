DROP PROCEDURE simsipeg.sp_GetKPI;
CREATE PROCEDURE simsipeg.sp_GetKPI()
BEGIN
    SELECT * FROM tbl_kpi kpi ORDER BY kpi.updated_date DESC;
END;