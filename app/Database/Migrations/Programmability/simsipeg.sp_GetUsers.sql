DROP PROCEDURE simsipeg.sp_GetUsers;
CREATE PROCEDURE simsipeg.sp_GetUsers(
    IN profile_in CHAR(1)
)
BEGIN
    select
        tu.*,
        tp.profile_name 
    from tbl_users tu 
    left join tbl_profiles tp on tp.profile_id = tu.profile 
    where tu.profile = CASE WHEN (profile_in IS NOT NULL AND profile_in <> '') THEN profile_in ELSE tu.profile END
    order by tu.updated_date desc;
END;