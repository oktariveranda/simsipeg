DROP PROCEDURE simsipeg.sp_AddKPIPegawai;
CREATE PROCEDURE simsipeg.sp_AddKPIPegawai(
    IN kpi_id_in INT,
    IN kpi_name_in NVARCHAR(500),
    IN user_id_in NVARCHAR(100),
    IN created_by_in NVARCHAR(100),
    IN score_in INT,
    IN year_in INT
)
BEGIN
    
    INSERT INTO tbl_assessments (kpi_id, kpi_name, user_id, score, year, created_by, created_date)
    VALUES (
        kpi_id_in,
        kpi_name_in,
        user_id_in,
        score_in,
        year_in,
        created_by_in,
        sysdate()
    );

    SELECT 'Success' Status FROM dual;

END;