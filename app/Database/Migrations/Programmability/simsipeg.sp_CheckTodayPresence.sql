DROP PROCEDURE simsipeg.sp_CheckTodayPresence;
CREATE PROCEDURE simsipeg.sp_CheckTodayPresence(IN user_id_in varchar(100))
BEGIN

    select 
        count(presence_date) total_presence
    from tbl_presence tp 
    inner join tbl_users tu on tu.user_id = tp.user_id and tu.is_deleted = 'N'
    where tp.user_id = user_id_in and cast(tp.presence_date as date) = current_date() group by tp.user_id;

END;