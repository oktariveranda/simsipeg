DROP PROCEDURE simsipeg.sp_UpdateSettings;
CREATE PROCEDURE simsipeg.sp_UpdateSettings(
    IN setting_id_in INT, 
    IN setting_value_in TIME,
    IN updated_by_in NVARCHAR(100)    
    )
BEGIN
    
    UPDATE tbl_settings SET 
    setting_value = CASE WHEN (setting_value_in IS NOT NULL AND setting_value_in <> '') THEN setting_value_in ELSE setting_value END,
    updated_by = CASE WHEN (updated_by_in IS NOT NULL AND updated_by_in <> '') THEN updated_by_in ELSE 'System' END,
    updated_date = sysdate()
    WHERE setting_id = setting_id_in;
    
    SELECT 'Success' Status FROM dual;
END;