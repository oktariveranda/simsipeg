DROP PROCEDURE simsipeg.sp_AddKPI;
CREATE PROCEDURE simsipeg.sp_AddKPI(
    IN kpi_name_in NVARCHAR(500),
    IN created_by_in NVARCHAR(100)
)
BEGIN
    
    INSERT INTO tbl_kpi (kpi_name, created_by, created_date)
    VALUES (kpi_name_in, created_by_in, sysdate());

    SELECT count(kpi_id) Total_KPI from tbl_kpi;
END;