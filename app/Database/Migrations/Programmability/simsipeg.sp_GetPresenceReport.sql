DROP PROCEDURE simsipeg.sp_GetPresenceReport;
CREATE PROCEDURE simsipeg.sp_GetPresenceReport(
    IN user_id_in NVARCHAR(100),
    IN month_in NVARCHAR(100),
    IN year_in INT(4)
)
BEGIN
	select 
        tp.status, tps.status_desc, count(tp.status) total_kehadiran, tp.user_id
    from tbl_presence tp 
    inner join tbl_presence_status tps on tps.status_code = tp.status
    where tp.user_id = user_id_in and year(presence_date) = year_in and month(presence_date) = month_in
    group by tp.status, tps.status_desc, tp.user_id;
END;