DROP PROCEDURE simsipeg.sp_GetStatus;
CREATE PROCEDURE simsipeg.sp_GetStatus()
BEGIN
    select * from tbl_presence_status tps ORDER BY tps.updated_date DESC;
END;