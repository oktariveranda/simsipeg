DROP PROCEDURE simsipeg.sp_GetKPIDetail;
CREATE PROCEDURE simsipeg.sp_GetKPIDetail(
    IN kpi_id_in NVARCHAR(100)
)
BEGIN
    SELECT * FROM tbl_kpi tk where tk.kpi_id = kpi_id_in;
END;