DROP PROCEDURE simsipeg.sp_AuthenticateUser;
CREATE PROCEDURE simsipeg.sp_AuthenticateUser(IN user_id_in varchar(100), IN password_in varchar(100))
BEGIN
    select profile, last_login, user_name 
    from tbl_users where user_id = user_id_in 
    and password = (case when (password_in is not null and password_in <> '') then password_in else password end)
    and is_deleted = 'N';
END;