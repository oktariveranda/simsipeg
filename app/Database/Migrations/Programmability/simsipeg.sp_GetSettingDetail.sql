DROP PROCEDURE simsipeg.sp_GetSettingDetail;
CREATE PROCEDURE simsipeg.sp_GetSettingDetail(
    IN setting_id_in INT
)
BEGIN
    SELECT * FROM tbl_settings ts where ts.setting_id = setting_id_in;
END;