DROP PROCEDURE simsipeg.sp_AddPresence;
CREATE PROCEDURE simsipeg.sp_AddPresence(
    IN user_id_in NVARCHAR(100),
    IN updated_by_in NVARCHAR(100),
    IN status_in CHAR(3)
)
BEGIN

    UPDATE tbl_users SET 
    updated_by = CASE WHEN (updated_by_in IS NOT NULL AND updated_by_in <> '') THEN updated_by_in ELSE updated_by END,
    updated_date = sysdate()
    WHERE user_id = user_id_in;

    INSERT INTO tbl_presence (presence_date, user_id, status) VALUES (sysdate(), user_id_in, status_in);

    SELECT 'Success' Status FROM dual;

END;