DROP PROCEDURE simsipeg.sp_GetKeterlambatan;
CREATE PROCEDURE simsipeg.sp_GetKeterlambatan(
    IN user_id_in NVARCHAR(100)
)
BEGIN
	select 
		sum(outer_res.is_late) total_record
	from (
		select 
			inner_res.user_id,
			inner_res.status,
			inner_res.presence_date,
			inner_res.latency_threshold,
			inner_res.time_only,
			(case when inner_res.time_only > inner_res.latency_threshold then 1 else 0 end) is_late
		from (
			select 
				user_id , status, presence_date, time(presence_date) time_only, 
				(
					select 
						date_add(convert(setting_value, time), interval 15 minute) 
					from tbl_settings ts
					where ts.setting_id = 1
				) latency_threshold 
			from tbl_presence tp where tp.user_id = user_id_in and (tp.status = 'H' OR tp.status = 'T')
			and month(CURRENT_DATE()) = month(presence_date)
		) inner_res
	) outer_res;	
END;