DROP PROCEDURE simsipeg.sp_GetAbsence;
CREATE PROCEDURE simsipeg.sp_GetAbsence(
    IN user_id_in NVARCHAR(100)
)
BEGIN
	select 
        count(tp.presence_id) total_record
    from tbl_presence tp where tp.user_id = user_id_in and tp.status = 'A'
    and month(CURRENT_DATE()) = month(presence_date);
END;