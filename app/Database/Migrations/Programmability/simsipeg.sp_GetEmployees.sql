DROP PROCEDURE simsipeg.sp_GetEmployees;
CREATE PROCEDURE simsipeg.sp_GetEmployees()
BEGIN
    select
        tu.*,
        tp.profile_name 
    from tbl_users tu 
    left join tbl_profiles tp on tp.profile_id = tu.profile 
    where tu.profile IN ('E', 'M') and tu.is_deleted = 'N'
    order by tu.updated_date desc;
END;