DROP PROCEDURE simsipeg.sp_UpdateUser;
CREATE PROCEDURE simsipeg.sp_UpdateUser(
    IN user_id_in NVARCHAR(100), 
    IN old_password_in NVARCHAR(100),
    IN password_in NVARCHAR(100),
    IN logout_in CHAR(1),
    IN user_name_in NVARCHAR(100),
    IN profile_in CHAR(1),
    IN is_deleted_in CHAR(1),
    IN updated_by_in NVARCHAR(100)
    )
BEGIN
    
    UPDATE tbl_users SET 
    password = CASE WHEN (password_in IS NOT NULL AND password_in <> '') THEN password_in ELSE password END,
    profile = CASE WHEN (profile_in IS NOT NULL AND profile_in <> '') THEN profile_in ELSE profile END,
    user_name = CASE WHEN (user_name_in IS NOT NULL AND user_name_in <> '') THEN user_name_in ELSE user_name END,
    is_deleted = CASE WHEN (is_deleted_in IS NOT NULL AND is_deleted_in <> '') THEN is_deleted_in ELSE is_deleted END,
    updated_by = CASE WHEN (updated_by_in IS NOT NULL AND updated_by_in <> '') THEN updated_by_in ELSE 'System' END,
    updated_date = sysdate()
    WHERE user_id = user_id_in;
    
    IF logout_in = 'Y' THEN
        UPDATE tbl_users SET 
        last_login = sysdate(),
        updated_by = CASE WHEN (updated_by_in IS NOT NULL AND updated_by_in <> '') THEN updated_by_in ELSE 'System' END,
        updated_date = sysdate()
        WHERE user_id = user_id_in;
    END IF;

    SELECT 'Success' Status FROM dual;
END;