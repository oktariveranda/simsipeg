DROP PROCEDURE simsipeg.sp_UpdateKPI;
CREATE PROCEDURE simsipeg.sp_UpdateKPI(
    IN kpi_id_in INT, 
    IN kpi_name_in NVARCHAR(500),
    IN updated_by_in NVARCHAR(100)
    )
BEGIN
    
    UPDATE tbl_kpi SET 
    kpi_name = CASE WHEN (kpi_name_in IS NOT NULL AND kpi_name_in <> '') THEN kpi_name_in ELSE kpi_name END,
    updated_by = CASE WHEN (updated_by_in IS NOT NULL AND updated_by_in <> '') THEN updated_by_in ELSE 'System' END,
    updated_date = sysdate()
    WHERE kpi_id = kpi_id_in;
    
    SELECT 'Success' Status FROM dual;
END;