<?php

namespace App\Models;

use CodeIgniter\Model;

class AssessmentModel extends Model
{
    protected $table      = 'tbl_assessment';
    protected $primaryKey = 'assessment_id';

    protected $useAutoIncrement = false;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $useTimestamps = false;
    
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function __construct()
    {
        $this->db = db_connect();
    }

    public function getKPIPegawai()
    {
        $result = $this->db->query("CALL sp_GetKPIPegawai()");
        return $result;
    }

    public function getKPIPegawaiDetail($userId, $year)
    {
        $result = $this->db->query("CALL sp_GetKPIPegawaiDetail('".$userId."', ".$year.")");
        return $result;
    }

    public function addKPIPegawai($kpiId, $kpiName, $userId, $createdBy, $score, $year)
    {
        $result = $this->db->query("CALL sp_AddKPIPegawai(".$kpiId.", '".$kpiName."', '".$userId."', '".$createdBy."', ".$score.", ".$year.")");
        return $result;
    }

}