<?php

namespace App\Models;

use CodeIgniter\Model;

class SettingModel extends Model
{
    protected $table      = 'tbl_settings';
    protected $primaryKey = 'setting_id';

    protected $useAutoIncrement = false;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $useTimestamps = false;
    protected $createdDate  = 'created_date';
    protected $createdBy  = 'created_by';
    protected $updatedDate  = 'updated_date';
    protected $updatedBy  = 'updated_by';
    protected $deletedField  = 'is_deleted';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function __construct()
    {
        $this->db = db_connect();
    }

    public function updateSetting($settingId, $settingValue, $createdBy)
    {
        $result = $this->db->query("CALL sp_UpdateSettings(".$settingId.", '".$settingValue."', '".$createdBy."')");
        return $result;
    }

    public function getSettingDetail($settingId)
    {
        $result = $this->db->query("CALL sp_GetSettingDetail(".$settingId.")");
        return $result;
    }

}