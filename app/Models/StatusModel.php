<?php

namespace App\Models;

use CodeIgniter\Model;

class StatusModel extends Model
{
    protected $table      = 'tbl_presence_status';
    protected $primaryKey = 'status_code';

    protected $useAutoIncrement = false;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $useTimestamps = false;
    protected $createdDate  = 'created_date';
    protected $createdBy  = 'created_by';
    protected $updatedDate  = 'updated_date';
    protected $updatedBy  = 'updated_by';
    protected $deletedField  = 'is_deleted';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function __construct()
    {
        $this->db = db_connect();
    }

    public function getStatusHadir()
    {
        $result = $this->db->query("CALL sp_GetStatus()");
        return $result;
    }
    
}