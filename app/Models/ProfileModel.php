<?php

namespace App\Models;

use CodeIgniter\Model;

class ProfileModel extends Model
{
    protected $table      = 'tbl_profiles';
    protected $primaryKey = 'profile_id';

    protected $useAutoIncrement = false;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $useTimestamps = false;
    protected $createdDate  = 'created_date';
    protected $createdBy  = 'created_by';
    protected $updatedDate  = 'updated_date';
    protected $updatedBy  = 'updated_by';
    protected $deletedField  = 'is_deleted';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function __construct()
    {
        $this->db = db_connect();
    }

    public function getProfiles()
    {
        $result = $this->db->query("CALL sp_GetProfiles()");
        return $result;
    }

}