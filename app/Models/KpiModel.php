<?php

namespace App\Models;

use CodeIgniter\Model;

class KpiModel extends Model
{
    protected $table      = 'tbl_kpi';
    protected $primaryKey = 'kpi_id';

    protected $useAutoIncrement = false;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $useTimestamps = false;
    
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function __construct()
    {
        $this->db = db_connect();
    }

    public function getKPI()
    {
        $result = $this->db->query("CALL sp_GetKPI()");
        return $result;
    }
    
    public function addKPI($kpiName, $createdBy)
    {
        $result = $this->db->query("CALL sp_AddKPI('".$kpiName."', '".$createdBy."')");
        return $result;
    }

    public function getKPIDetail($kpiId)
    {
        $result = $this->db->query("CALL sp_getKPIDetail('".$kpiId."')");
        return $result;
    }

    public function updateKPI($kpiId, $kpiName, $updatedBy = 'System')
    {
        $result = $this->db->query("CALL sp_UpdateKPI(".$kpiId.", '".$kpiName."', '".$updatedBy."');");
        return $result;
    }
}