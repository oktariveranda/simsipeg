<?php

namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table      = 'tbl_users';
    protected $primaryKey = 'user_id';

    protected $useAutoIncrement = false;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $useTimestamps = false;
    protected $createdDate  = 'created_date';
    protected $createdBy  = 'created_by';
    protected $updatedDate  = 'updated_date';
    protected $updatedBy  = 'updated_by';
    protected $deletedField  = 'is_deleted';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function __construct()
    {
        $this->db = db_connect();
    }

    public function authenticateUser($userId, $password)
    {
        $result = $this->db->query("CALL sp_AuthenticateUser('".$userId."', '".$password."');");
        return $result;
    }

    public function updateUser(
        $userId, $oldPassword, $newPassword, $logoutFlag = 'N', 
        $userName = '', $profile = '', $isDeleted = 'N', $updatedBy = 'System'
        )
    {
        $result = $this->db->query("CALL sp_UpdateUser('".$userId."', '".$oldPassword."', '".$newPassword."', '".$logoutFlag."', '".$userName."', '".$profile."', '".$isDeleted."', '".$updatedBy."');");
        return $result;
    }

    public function addUser($userId, $userName, $password, $profile, $createdBy)
    {
        $result = $this->db->query("CALL sp_AddUser('".$userId."', '".$userName."', '".$password."', '".$profile."', '".$createdBy."')");
        return $result;
    }

    public function getUsers($profile = NULL)
    {
        $result = $this->db->query("CALL sp_GetUsers('".$profile."')");
        return $result;
    }

    public function getEmployees()
    {
        $result = $this->db->query("CALL sp_GetEmployees()");
        return $result;
    }

    public function getUserDetail($userId = '')
    {
        $result = $this->db->query("CALL sp_GetUserDetail('".$userId."')");
        return $result;
    }

}