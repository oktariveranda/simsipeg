<?php

namespace App\Models;

use CodeIgniter\Model;

class PresenceModel extends Model
{
    protected $table      = 'tbl_presence';
    protected $primaryKey = 'presence_id';

    protected $useAutoIncrement = false;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $useTimestamps = false;
    
    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

    public function __construct()
    {
        $this->db = db_connect();
    }

    public function updatePresence($userId, $updatedBy, $status)
    {
        $result = $this->db->query("CALL sp_AddPresence('".$userId."', '".$updatedBy."', '".$status."');");
        return $result;
    }

    public function getUsersPresence($profile = NULL)
    {
        $result = $this->db->query("CALL sp_GetUsersPresence('".$profile."');");
        return $result;
    }

    public function getUserDetailPresence($userId)
    {
        $result = $this->db->query("CALL sp_GetUserDetailPresence('".$userId."');");
        return $result;
    }

    public function getUserHistoricalPresence($userId = '')
    {
        $result = $this->db->query("CALL sp_GetUserHistoricalPresence('".$userId."');");
        return $result;
    }

    public function getTotalKeterlambatan($userId = '')
    {
        $result = $this->db->query("CALL sp_GetKeterlambatan('".$userId."');");
        return $result;
    }

    public function getTotalAbsence($userId = '')
    {
        $result = $this->db->query("CALL sp_GetAbsence('".$userId."');");
        return $result;
    }

    public function checkTodayPresence($userId = null)
    {
        $result = $this->db->query("CALL sp_CheckTodayPresence('".$userId."');");
        return $result;
    }

    public function getOvertime($userId = null)
    {
        $result = $this->db->query("CALL sp_GetOvertime('".$userId."');");
        return $result;
    }

    public function getPresenceReport($userId = null, $month, $year)
    {
        $result = $this->db->query("CALL sp_GetPresenceReport('".$userId."', ".$month.", ".$year.");");
        return $result;
    }

    public function getOvertimeReport($userId = null, $month, $year)
    {
        $result = $this->db->query("CALL sp_GetOvertimeReport('".$userId."', ".$month.", ".$year.");");
        return $result;
    }

}