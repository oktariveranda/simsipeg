<?php 

namespace App\Libraries;

use CodeIgniter\I18n\Time;

class Site {

    var $defaultPage = '';

	public function viewPage($page = 'user/index', $hasSidebar = true, $data = array())
	{
		echo view('templates/header');
		if($hasSidebar)
		{
			echo view('templates/sidebar');
		}
		echo view($page, $data);
        echo view('templates/footer');
	}

	public function encodePassword($password_salt, $password)
	{
		return sha1($password_salt.$password);
	}

	public function validateQueryResult($object = null)
	{
		return (isset($object) && $object->getNumRows() > 0);
	}

	public function convertDate($date = null)
	{
		$result = null;
		if($date != null)
		{
			$result = date_create($date);
			$result = date_format($result, 'd-M-Y H:i:s');
		}
		return $result;
	}

	public function addMinutesToTime( $hour, $minute, $second) 
	{
		return Time::createFromTime($hour, $minute, $second); // 11:30 am today
	}

	public function getMonths()
	{
		return array(
			1 => "Januari",
			2 => "Februari",
			3 => "Maret",
			4 => "April",
			5 => "Mei",
			6 => "Juni",
			7 => "Juli",
			8 => "Agustus",
			9 => "September",
			10 => "Oktober",
			11 => "Nopember",
			12 => "Desember"
		);
	}

	public function getYears($since = 2020)
	{
		$years = array();
		for($year = $since; $year <= date("Y"); $year++)
		{
			array_push($years, $year);
		}
		return $years;
	}

}