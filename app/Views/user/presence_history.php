<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Riwayat Kehadiran</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('/') ?>">Home</a></li>
              <li class="breadcrumb-item active">Riwayat Kehadiran</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="alert alert-secondary alert-dismissible fade show" role="alert">
                Jam Operasional (<?= $awal['setting_value'] ?> - <?= $akhir['setting_value'] ?>)
            </div>
          </div>
        </div>
        <div class="row">
            <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Riwayat Kehadiran</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="presence-history" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>No.</th>
                    <?php
                    if(session()->get('profile') == PROFILE_MANAGER || session()->get('profile') == PROFILE_OWNER)
                    {
                      ?>
                      <th>Nama Pegawai</th>
                      <th>Profile</th>
                      <?php
                    }
                    ?>
                    <th>Tgl Hadir</th>
                    <th>Status Hadir</th>
                    <th>Tgl Pulang</th>
                    <th>Status Pulang</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php
                    if($list_presence != null)
                    {
                      $counter = 1;
                      foreach($list_presence as $presence)
                      {
                        ?>
                        <tr>
                          <td><?= $counter++ ?></td>
                          <?php
                          if(session()->get('profile') == PROFILE_MANAGER || session()->get('profile') == PROFILE_OWNER)
                          {
                            ?>
                            <td><?= $presence['user_name'] ?></td>
                            <td><?= $presence['profile'] ?></td>                            
                            <?php
                          }
                          ?>
                          <td><?= ($presence['hadir_date'] <> '' && $presence['hadir_date'] != null) ? date_format(date_create($presence['hadir_date']), 'd-M-y H:i:s') : '-' ?></td>
                          <td><?= ($presence['hadir_status_desc'] <> '' && $presence['hadir_status_desc'] != null) ? $presence['hadir_status_desc'] : '-' ?></td>
                          <td><?= ($presence['hadir_date'] <> '' && $presence['pulang_date'] != null) ? date_format(date_create($presence['pulang_date']), 'd-M-y H:i:s') : '-' ?></td>
                          <td><?= ($presence['pulang_status_desc'] <> '' && $presence['pulang_status_desc'] != null) ? $presence['pulang_status_desc'] : '-' ?></td>
                        </tr>
                        <?php
                      }
                    }
                    ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>No.</th>
                    <?php
                    if(session()->get('profile') == PROFILE_MANAGER || session()->get('profile') == PROFILE_OWNER)
                    {
                      ?>
                      <th>Nama Pegawai</th>
                      <th>Profile</th>
                      <?php
                    }
                    ?>
                    <th>Tgl Hadir</th>
                    <th>Status Hadir</th>
                    <th>Tgl Pulang</th>
                    <th>Status Pulang</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            </div>    
        </div>
        <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Riwayat Lembur</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="overtime-history" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>No.</th>
                    <?php
                    if(session()->get('profile') == PROFILE_MANAGER || session()->get('profile') == PROFILE_OWNER)
                    {
                      ?>
                      <th>Nama Pegawai</th>
                      <th>Profile</th>
                      <?php
                    }
                    ?>
                    <th>Tanggal</th>
                    <th>Lembur</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php
                    if($list_overtime != null)
                    {
                      $counter = 1;
                      foreach($list_overtime as $overtime)
                      {
                        ?>
                        <tr>
                          <td><?= $counter++ ?></td>
                          <?php
                          if(session()->get('profile') == PROFILE_MANAGER || session()->get('profile') == PROFILE_OWNER)
                          {
                            ?>
                            <td><?= $overtime['user_name'] ?></td>
                            <td><?= $overtime['profile'] ?></td>                            
                            <?php
                          }
                          ?>
                          <td><?= date_format(date_create($overtime['presence_date_tap_out']), 'd-M-y') ?></td>
                          <td><?= $overtime['time_difference'] ?></td>
                        </tr>
                        <?php
                      }
                    }
                    ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>No.</th>
                    <?php
                    if(session()->get('profile') == PROFILE_MANAGER || session()->get('profile') == PROFILE_OWNER)
                    {
                      ?>
                      <th>Nama Pegawai</th>
                      <th>Profile</th>
                      <?php
                    }
                    ?>
                    <th>Tanggal</th>
                    <th>Lembur</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            </div>    
        <!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->