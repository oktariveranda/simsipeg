<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profilku</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= base_url('/') ?>">Home</a></li>
              <li class="breadcrumb-item active">Profilku</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
          <div class="card card-secondary">
              <?php if (!empty(session()->getFlashdata('error'))) : ?>
                  <div class="alert alert-danger alert-dismissible fade show" role="alert">
                      <?php echo session()->getFlashdata('error'); ?>
                  </div>
              <?php endif; ?>
              <?php if (!empty(session()->getFlashdata('info'))) : ?>
                  <div class="alert alert-info alert-dismissible fade show" role="alert">
                      <?php echo session()->getFlashdata('info'); ?>
                  </div>
              <?php endif; ?>
              <div class="card-header">
                <h3 class="card-title">Profilku</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="<?= base_url().'/change_password' ?>" method="POST">
                <div class="card-body">
                  <div class="form-group">
                    <label for="labelNik">NIK</label>
                    <input type="text" class="form-control" id="labelNik" disabled="true" value="<?= $_SESSION['user_id'] ?>">
                    <input type="hidden" id="hiddenNik" name="hiddenNik" value="<?= $_SESSION['user_id'] ?>" />
                  </div>
                  <div class="form-group">
                    <label for="changePasswordLama">Password Lama</label>
                    <input type="password" class="form-control" id="password_lama" name="password_lama" placeholder="Password Lama">
                  </div>
                  <div class="form-group">
                    <label for="changePasswordBaru">Password Baru</label>
                    <input type="password" class="form-control" id="password_baru" name="password_baru" placeholder="Password Baru">
                  </div>
                  <div class="form-group">
                    <label for="changePasswordBaru2">Konfirmasi Password Baru</label>
                    <input type="password" class="form-control" id="confirm_password_baru" name="confirm_password_baru" placeholder="Password Baru">
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-secondary">Submit</button>
                </div>
              </form>
            </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->