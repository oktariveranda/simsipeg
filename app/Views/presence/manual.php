<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Presensi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('/') ?>">Home</a></li>
              <li class="breadcrumb-item active">Input Manual Presensi</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
              <?php if (!empty(session()->getFlashdata('error'))) : ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('error'); ?>
                </div>
                <?php endif; ?>
                <?php if (!empty(session()->getFlashdata('info'))) : ?>
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('info'); ?>
                </div>
              <?php endif; ?>
              <?php
              if($action == 'add')
              {
                ?>
                <div class="card card-secondary">
                <div class="card-header">
                  <h3 class="card-title">Input Manual Presensi Pegawai</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form class="form-horizontal" action="<?= base_url('/manual_input') ?>" method="POST">
                  <div class="card-body">
                    <div class="form-group row">
                      <label for="nik" class="col-sm-2 col-form-label">NIK</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="nik" name="nik" placeholder="NIK Pegawai Baru" required value="<?= $user_detail['user_id'] ?>" disabled>
                        <input type="hidden" id="hiddenNik" name="hiddenNik" value="<?= $user_detail['user_id'] ?>" />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="userName" class="col-sm-2 col-form-label">Nama Pegawai</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="userName" name="userName" placeholder="Nama Pegawai Baru" required value="<?= $user_detail['user_name'] ?>" disabled>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="profile" class="col-sm-2 col-form-label">Profil</label>
                      <div class="col-sm-10">
                        <select class="form-control" id="profile" name="profile" required disabled>
                          <?php
                          if(!is_null($list_profiles))
                          {
                            ?>
                            <option value="0">Pilih profile...</option>
                            <?php
                            foreach($list_profiles as $row)
                            {
                              ?>
                              <option value="<?= $row['profile_id'] ?>" <?= ($row['profile_id'] == $user_detail['profile']) ? 'selected': '' ?>><?= $row['profile_id'].' - '.$row['profile_name'] ?></option>
                              <?php
                            }
                          }
                          else
                          {
                            ?><option value="0">Tidak ada data</option><?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="presenceStatus" class="col-sm-2 col-form-label">Kehadiran</label>
                        <div class="col-sm-10">
                          <select class="form-control" id="presenceStatus" name="presenceStatus" required>
                            <?php
                            if(!is_null($list_status))
                            {
                              ?>
                              <option value="0">Pilih status kehadiran...</option>
                              <?php
                              foreach($list_status as $row)
                              {
                                ?>
                                <option value="<?= $row['status_code'] ?>" <?= ($row['status_code'] == $user_detail['recent_status']) ? 'selected': '' ?>><?= $row['status_code'].' - '.$row['status_desc'] ?></option>
                                <?php
                              }
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                  </div>
                  <!-- /.card-body -->
                  <?php
                      if($user_detail['is_deleted'] == 'N')
                      {
                        $last_presence_date = (!is_null($user_detail['last_presence_date'])) ? date_format(date_create($user_detail['last_presence_date']), 'd-m-Y') : '';
                        $current_date = date('d-m-Y');
                        if($last_presence_date == $current_date)
                        {
                          // nothing to show
                        }
                        else
                        {
                          ?>
                          <div class="card-footer">
                            <button type="submit" class="btn btn-secondary">Submit</button>
                            <button type="reset" class="btn btn-default float-right">Batal</button>
                          </div>
                          <?php
                        }
                      }
                      else
                      {
                        // nothing to show
                      }
                    ?>
                  <!-- /.card-footer -->
                </form>
              </div>
                <?php
              }
              ?>
              <div class="card card-secondary">
                  <div class="card-header">
                  <h3 class="card-title">Status kehadiran pegawai per tanggal <?= date_format(date_create(date('Y-m-d')), 'd-M-Y') ?></h3>
              </div>
                <div class="card-body">
                <table id="pengaturan-status" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>Nama Pegawai</th>
                    <th>Profile</th>
                    <th>Kehadiran</th>
                    <th>Tanggal Kehadiran Terakhir</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php 
                    if(!is_null($list_users))
                    {
                      $counter = 1;
                      foreach($list_users as $row)
                      {
                        ?>
                        <tr>
                          <td><?= $counter++ ?></td>
                          <td><?= $row['user_name']  ?>
                          <td><?= $row['profile']  ?>
                          <td><?= $row['status_code'].' - '.$row['status_desc'] ?>
                          <td><?= ($row['last_presence_date'] != null) ? date_format(date_create($row['last_presence_date']), 'd-M-Y h:i:s') : '' ?>
                          <td><?= ($row['is_deleted'] == 'N') ? 'Aktif' : 'Tidak Aktif'  ?>
                          <td>
                            <?php
                              if($row['is_deleted'] == 'N')
                              {
                                $last_presence_date = (!is_null($row['last_presence_date'])) ? date_format(date_create($row['last_presence_date']), 'd-m-Y') : '';
                                $current_date = date('d-m-Y');
                                if($last_presence_date == $current_date)
                                {
                                  ?>Not Available<?php
                                }
                                else
                                {
                                  ?>
                                  <a href="<?= base_url('/manual_input/'.$row['user_id']) ?>" class="btn btn-secondary" role="button">Input Kehadiran</a>
                                  <?php
                                }
                              }
                              else
                              {
                                ?>Not Available<?php
                              }
                            ?>
                          </td>
                        </tr>
                        <?php
                      }
                    }
                    ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>No.</th>
                    <th>Nama Pegawai</th>
                    <th>Profile</th>
                    <th>Kehadiran</th>
                    <th>Tanggal Kehadiran Terakhir</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->