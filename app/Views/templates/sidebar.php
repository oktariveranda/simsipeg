<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?= base_url('/') ?>" class="nav-link">Home</a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?= base_url('/') ?>" class="brand-link">
      <img src="<?= base_url('assets/dist') ?>/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">SimSiPeg v1.0</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
          <a href="#" class="d-block"><?= session()->get('user_id').' - '.session()->get('user_name') ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <?php
          if(session()->get('profile') == PROFILE_MANAGER || session()->get('profile') == PROFILE_OWNER)
          {
            ?>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                  KPI
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <?php
                if(session()->get('profile') == PROFILE_MANAGER)
                {
                  ?>
                  <li class="nav-item">
                    <a href="<?= base_url('/kpi') ?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Komponen KPI</p>
                    </a>
                  </li>
                  <?php
                }
                ?>
                  <li class="nav-item">
                    <a href="<?= base_url('/input_kpi') ?>" class="nav-link">
                      <i class="far fa-circle nav-icon"></i>
                      <p>KPI Pegawai</p>
                    </a>
                  </li>
                <!-- <li class="nav-item">
                  <a href="<?= base_url('/kpi_history') ?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Riwayat KPI</p>
                  </a>
                </li> -->
              </ul>
            </li>
            <?php
          }
          ?>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Pengaturan
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <?php
              if(session()->get('profile') == 'A')
              {
                ?>
                <li class="nav-item">
                  <a href="<?= base_url('/user_setting') ?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>User</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?= base_url('/working_setting') ?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Jam Operasional</p>
                  </a>
                </li>
                <!-- <li class="nav-item">
                  <a href="<?= base_url('/status_setting') ?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Pengaturan Status</p>
                  </a>
                </li> -->
                <?php
              }
              ?>
              <li class="nav-item">
                <a href="<?= base_url('/profile') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Profilku</p>
                </a>
              </li>
            </ul>
          </li>
          <?php
          if(session()->get('profile') == PROFILE_EMPLOYEE || session()->get('profile') == PROFILE_MANAGER || session()->get('profile') == PROFILE_OWNER)
          {
            ?>
            <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-calendar-alt"></i>
              <p>
                Presensi
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <?php
              if(session()->get('profile') == PROFILE_MANAGER)
              {
                ?>
                <li class="nav-item">
                  <a href="<?= base_url('/manual_input') ?>" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Input Manual</p>
                  </a>
                </li>
                <?php
              }
              ?>
              <li class="nav-item">
                <a href="<?= base_url('/presence_history') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Riwayat Kehadiran</p>
                </a>
              </li>
            </ul>
            </li>
            <?php
          }
          ?>
          <?php
          if(session()->get('profile') == PROFILE_MANAGER || session()->get('profile') == PROFILE_OWNER)
          {
            ?>
            <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                Laporan
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url('/report/kehadiran') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Kehadiran</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url('/report/lembur') ?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Lembur</p>
                </a>
              </li>
            </ul>
            <?php
          }
          ?>
          <li class="nav-item">
            <a href="<?= base_url('/index.php/logout') ?>" class="nav-link">
              <i class="nav-icon fas fa-power-off"></i>
              <p>
                Logout
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>