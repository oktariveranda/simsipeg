<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>KPI</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('/') ?>">Home</a></li>
              <li class="breadcrumb-item active">KPI</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
            <div class="col-12">
              <?php if (!empty(session()->getFlashdata('error'))) : ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('error'); ?>
                </div>
                <?php endif; ?>
                <?php if (!empty(session()->getFlashdata('info'))) : ?>
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('info'); ?>
                </div>
              <?php endif; ?>
              <?php
              if($action == 'input_kpi')
              {
                ?>
                <div class="card card-secondary">
                  <div class="card-header">
                    <h3 class="card-title">Input KPI Pegawai <?= ($user_name != '') ? '- '.$user_name : '' ?></h3>
                  </div>
                  <!-- /.card-header -->
                  <!-- form start -->
                  <form class="form-horizontal" method="POST" action="<?= base_url('/input_kpi') ?>">
                    <div class="card-body">
                      <div class="form-group row">
                        <label for="inputKategori1" class="col-sm-10 col-form-label">Periode</label>
                        <div class="col-sm-2">
                          <input type="text" class="form-control" id="periode" name="periode" value="<?= date('Y') ?>" disabled>
                          <input type="hidden" id="hiddenYear" name="hiddenYear" value="<?= date('Y') ?>"/>
                          <input type="hidden" id="hiddenUserId" name="hiddenUserId" value="<?= $user_id ?>"/>
                        </div>
                      </div>
                      <?php
                      if(!is_null($list_kpi) && isset($list_kpi))
                      {
                        $counter = 1;
                        foreach($list_kpi as $kpi)
                        {
                        ?>
                        <div class="form-group row">
                          <label for="inputKategori1" class="col-sm-10 col-form-label"><?= $counter++.'. '.$kpi['kpi_name'] ?></label>
                          <div class="col-sm-2">
                            <input type="number" class="form-control" id="<?= 'score_'.$kpi['kpi_id'] ?>" name="<?= 'score_'.$kpi['kpi_id'] ?>" min="<?= MINIMUM_KPI ?>" max="<?= MAXIMUM_KPI ?>" required />
                            <input type="hidden" id="hiddenKPIId_<?= $kpi['kpi_id'] ?>" name="hiddenKPIId_<?= $kpi['kpi_id'] ?>" value="<?= $kpi['kpi_id'] ?>"/>
                            <input type="hidden" id="hiddenKPIName_<?= $kpi['kpi_id'] ?>" name="hiddenKPIName_<?= $kpi['kpi_id'] ?>" value="<?= $kpi['kpi_name'] ?>"/>
                          </div>
                        </div>
                        <?php
                        }
                      }
                      ?>
                    </div>
                    <div class="card-footer">
                      <button type="submit" class="btn btn-secondary">Submit</button>
                      <button type="reset" class="btn btn-default float-right">Batal</button>
                    </div>
                  </form>
                </div>
                <?php
              }
              else if($action == 'view_kpi')
              {
                ?>
                <div class="card card-secondary">
                  <div class="card-header">
                  <h3 class="card-title">KPI Pegawai <?= ($user_name != '') ? '- '.$user_name : '' ?></h3>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                      <div class="form-group row">
                        <label for="inputKategori1" class="col-sm-10 col-form-label">Periode</label>
                        <label class="col-sm-2 col-form-label"><?= $year ?></label>
                      </div>
                      <?php
                      if(!is_null($assessment_list) && isset($assessment_list))
                      {
                        $counter = 1;
                        foreach($assessment_list as $assessment)
                        {
                        ?>
                        <div class="form-group row">
                          <label for="inputKategori1" class="col-sm-10 col-form-label"><?= $counter++.'. '.$assessment['kpi_name'] ?></label>
                          <label class="col-sm-2 col-form-label"><?= $assessment['score'] ?></label>
                        </div>
                        <?php
                        }
                      }
                      ?>
                    </div>
                </div>
                <?php
              }
              ?>
              <div class="card">
              <div class="card-header">
                <h3 class="card-title">KPI Pegawai</h3>
              </div>
              <div class="card-body">
                <div class="row">
                  <!-- ./col -->
                <div class="col-lg-3 col-6">
                  <!-- small box -->
                  <div class="small-box bg-danger">
                    <div class="inner">
                      <h3>Skor 1 - 2</h3>
                      <p>Sangat Buruk</p>
                    </div>
                  </div>
                </div>
                <!-- ./col -->
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                  <!-- small box -->
                  <div class="small-box bg-warning">
                    <div class="inner">
                      <h3>Skor 2 - 3</h3>
                      <p>Buruk</p>
                    </div>
                  </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-6">
                  <!-- small box -->
                  <div class="small-box bg-success">
                    <div class="inner">
                      <h3>Skor 3 - 4</h3>
                      <p>Baik</p>
                    </div>
                  </div>
                </div>
                <div class="col-lg-3 col-6">
                  <!-- small box -->
                  <div class="small-box bg-info">
                    <div class="inner">
                      <h3>Skor 4 - 5</h3>
                      <p>Sangat Baik</p>
                    </div>
                  </div>
                </div>
              </div>
                <table id="pengaturan-user" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>NIK</th>
                    <th>Nama Pegawai</th>
                    <th>Status</th>
                    <th>Skor</th>
                    <th>Periode</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php 
                    if(!is_null($list_users))
                    {
                      $counter = 1;
                      foreach($list_users as $row)
                      {
                        ?>
                        <tr>
                          <td><?= $counter++ ?></td>
                          <td><?= $row['user_id'] ?></td>
                          <td><?= $row['user_name'] ?>
                          <td><?= $row['is_deleted'] ?>
                          <td><?= $row['score'] ?>
                          <td><?= $row['year'] ?>
                          <td>
                            <?php
                            if(strtolower($row['is_deleted']) == strtolower('Aktif') && (is_null($row['year']) || empty($row['year']) || $row['year'] == '') && session()->get('profile') == PROFILE_MANAGER)
                            {
                              ?>
                              <a href="<?= base_url('/input_kpi/'.$row['user_id']) ?>" class="btn btn-secondary" role="button">Input KPI</a>
                              <?php
                            }
                            else if(($row['year'] == date('Y') || $row['score'] != null))
                            {
                              ?>
                              <a href="<?= base_url('/view_kpi/'.$row['user_id'].'/'.$row['year']) ?>" class="btn btn-secondary" role="button">View KPI</a>
                              <?php
                            }
                            ?>
                          </td>
                        </tr>
                        <?php
                      }
                    }
                    ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>No.</th>
                    <th>NIK</th>
                    <th>Nama Pegawai</th>
                    <th>Profil</th>
                    <th>Terakhir Login</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
        <!-- /.container-fluid -->
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->