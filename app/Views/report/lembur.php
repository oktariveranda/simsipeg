<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Report</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('/') ?>">Home</a></li>
              <li class="breadcrumb-item active">Lembur</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
            <div class="col-12">
              <?php if (!empty(session()->getFlashdata('error'))) : ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('error'); ?>
                </div>
                <?php endif; ?>
                <?php if (!empty(session()->getFlashdata('info'))) : ?>
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('info'); ?>
                </div>
              <?php endif; ?>
              <div class="card card-secondary">
                <div class="card-header">
                  <h3 class="card-title">Laporan Lembur</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="<?= base_url('/report/lembur') ?>">
                  <div class="card-body">
                    <div class="form-group row">
                        <label for="inputKategori1" class="col-sm-2 col-form-label">Periode</label>
                        <div class="col-sm-6">
                          <div class="input-group">
                            <select class="form-control" id="month" name="month" required>
                              <?php
                              if(!is_null($months))
                              {
                                ?>
                                <option value="0">Pilih periode bulan...</option>
                                <?php
                                foreach($months as $key => $value)
                                {
                                  ?>
                                  <option value="<?= $key ?>"><?= $value ?></option>
                                  <?php
                                }
                              }
                              ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-sm-4">
                          <div class="input-group">
                            <select class="form-control" id="year" name="year" required>
                              <?php
                              if(!is_null($years))
                              {
                                ?>
                                <option value="0">Pilih periode tahun...</option>
                                <?php
                                foreach($years as $key => $value)
                                {
                                  ?>
                                  <option value="<?= $value ?>"><?= $value ?></option>
                                  <?php
                                }
                              }
                              ?>
                            </select>
                          </div>
                        </div>
                    </div>
                  </div>
                  <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-secondary">Submit</button>
                        <button type="reset" class="btn btn-default float-right">Batal</button>
                    </div>
                  <!-- /.card-footer -->
                </form>
              </div>  
              <?php
              if($report != null && !is_null($report))
              {
                ?>
                  <div class="card card-secondary">
                    <div class="card-header">
                      <h3 class="card-title">Report per <?= $months[$selected_month] .' '.$selected_year ?></h3>
                    </div>
                    <div class="card-body">
                      <div class="card">
                        <div class="card-header border-transparent">
                          <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                              <i class="fas fa-minus"></i>
                            </button>
                            <button type="button" class="btn btn-tool" data-card-widget="remove">
                              <i class="fas fa-times"></i>
                            </button>
                          </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0">
                          <div class="table-responsive">
                            <table class="table m-0">
                              <thead>
                              <tr>
                                <th>No.</th>
                                <th>Pegawai</th>
                                <th>Total Lembur</th>
                              </tr>
                              </thead>
                              <tbody>
                                <?php 
                                $count = 1;
                                foreach($report as $row)
                                {
                                  ?>
                                  <tr>
                                    <td><?= $count++ ?></td>
                                    <td><?= $row['user_id'] .' - '.$row['user_name'] ?></td>
                                    <td>
                                      <div class="sparkbar" data-color="#00a65a" data-height="20"><?= $row['total_overtime'] . ' jam' ?></div>
                                    </td>
                                  </tr>
                                  <?php
                                }
                                ?>
                              </tbody>
                            </table>
                          </div>
                          <!-- /.table-responsive -->
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer clearfix">                          
                        </div>
                        <!-- /.card-footer -->
                      </div>
                      <!-- /.card -->
                    </div>
                    </div>
                <?php
              }
              else
              {
                if($isProcessed == true)
                {
                  ?>
                    <div class="card card-secondary">
                        <div class="card-header">
                        <h3 class="card-title">Report per <?= $months[$selected_month] .' '.$selected_year ?></h3>
                        </div>
                        <div class="card-body">
                          <div class="card">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                              Tidak ada data
                            </div>
                          </div>
                        </div>
                    </div>
                    <?php
                }
              }
              ?>
            </div>
          </div>
        </div>
        <!-- /.row -->
        <!-- /.container-fluid -->
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->