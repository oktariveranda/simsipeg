<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Pengaturan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('/') ?>">Home</a></li>
              <li class="breadcrumb-item active">Pengaturan User</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
          <?php if (!empty(session()->getFlashdata('error'))) : ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?php echo session()->getFlashdata('error'); ?>
            </div>
            <?php endif; ?>
            <?php if (!empty(session()->getFlashdata('info'))) : ?>
            <div class="alert alert-info alert-dismissible fade show" role="alert">
                <?php echo session()->getFlashdata('info'); ?>
            </div>
          <?php endif; ?>
          <div class="card card-secondary">
                <div class="card-header">
                  <h3 class="card-title"><?= ($action == 'add') ? 'Tambah Pegawai' : 'Ubah Pegawai' ?></h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form class="form-horizontal" action="<?= ($action == 'add') ? base_url('/add_user') : base_url('/update_user') ?>" method="POST">
                  <div class="card-body">
                    <div class="form-group row">
                      <label for="nik" class="col-sm-2 col-form-label">NIK</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="nik" name="nik" placeholder="NIK Pegawai Baru" required value="<?= ($action == 'add') ? '' : $user_detail['user_id'] ?>" <?= ($action == 'add') ? '' : 'disabled' ?>>
                        <?php if($action == 'edit') { ?><input type="hidden" id="hiddenNik" name="hiddenNik" value="<?= $user_detail['user_id'] ?>" /><?php } ?>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="userName" class="col-sm-2 col-form-label">Nama Pegawai</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="userName" name="userName" placeholder="Nama Pegawai Baru" required value="<?= ($action == 'add') ? '' : $user_detail['user_name'] ?>">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="profile" class="col-sm-2 col-form-label">Profil</label>
                      <div class="col-sm-10">
                        <select class="form-control" id="profile" name="profile" required>
                          <?php
                          if(!is_null($list_profiles))
                          {
                            ?>
                            <option value="0">Pilih profile...</option>
                            <?php
                            foreach($list_profiles as $row)
                            {
                              if($action == 'add')
                              {
                                ?>
                                <option value="<?= $row['profile_id'] ?>"><?= $row['profile_name'].' - '.$row['profile_name'] ?></option>
                                <?php
                              }
                              else
                              {
                                ?>
                                <option value="<?= $row['profile_id'] ?>" <?= ($row['profile_id'] == $user_detail['profile']) ? 'selected': '' ?>><?= $row['profile_name'].' - '.$row['profile_name'] ?></option>
                                <?php
                              }
                            }
                          }
                          else
                          {
                            ?><option value="0">Tidak ada data</option><?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <?php
                    if($action == 'edit')
                    {
                      ?>
                      <div class="form-group row">
                        <label for="isDeleted" class="col-sm-2 col-form-label">Status</label>
                        <div class="col-sm-10">
                          <select class="form-control" id="isDeleted" name="isDeleted" required>
                              <option value="N" <?= ($user_detail['is_deleted'] == 'N') ? 'selected' : '' ?>>Aktif</option>
                              <option value="Y" <?= ($user_detail['is_deleted'] == 'Y') ? 'selected' : '' ?>>Tidak Aktif</option>
                          </select>
                        </div>
                      </div>
                      <?php
                    }
                    ?>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-secondary">Submit</button>
                    <button type="reset" class="btn btn-default float-right">Batal</button>
                  </div>
                  <!-- /.card-footer -->
                </form>
              </div>
              <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">List User/Pegawai</h3>
              </div>
            <div class="card-body">
                <table id="pengaturan-status" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>NIK</th>
                    <th>Nama Pegawai</th>
                    <th>Profil</th>
                    <th>Terakhir Login</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php 
                    if(!is_null($list_users))
                    {
                      $counter = 1;
                      foreach($list_users as $row)
                      {
                        ?>
                        <tr>
                          <td><?= $counter++ ?></td>
                          <td><?= $row['user_id'] ?></td>
                          <td><?= $row['user_name']  ?>
                          <td><?= $row['profile_name']  ?>
                          <td><?= $row['last_login']  ?>
                          <td><?= ($row['is_deleted'] == 'N') ? 'Aktif' : 'Tidak Aktif'  ?>
                          <td><a href="<?= base_url('/edit_user/'.$row['user_id']) ?>" class="btn btn-secondary" role="button">Update</a></td>
                        </tr>
                        <?php
                      }
                    }
                    ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>No.</th>
                    <th>NIK</th>
                    <th>Nama Pegawai</th>
                    <th>Profil</th>
                    <th>Terakhir Login</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->