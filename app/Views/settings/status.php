<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Pengaturan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('/') ?>">Home</a></li>
              <li class="breadcrumb-item active">Pengaturan Status</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card card-warning">
                <div class="card-header">
                  <h3 class="card-title">Pengaturan Status</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="<?= base_url('/add_status') ?>">
                  <div class="card-body">
                    <div class="form-group row">
                      <label for="inputStatusCode" class="col-sm-2 col-form-label">Kode Status</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputStatusCode" placeholder="Kode Status">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="inputStatusDesc" class="col-sm-2 col-form-label">Status</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputStatusDesc" placeholder="Status">
                      </div>
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-warning">Tambah</button>
                    <button type="submit" class="btn btn-default float-right">Batal</button>
                  </div>
                  <!-- /.card-footer -->
                </form>
              </div>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List Status</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="pengaturan-status" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>Kode Status</th>
                    <th>Deskripsi Status</th>
                    <th>Dibuat oleh</th>
                    <th>Dibuat tanggal</th>
                    <th>Diubah oleh</th>
                    <th>Diubah tanggal</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php 
                    if(!is_null($list_status))
                    {
                      $counter = 1;
                      foreach($list_status as $row)
                      {
                        ?>
                        <tr>
                          <td><?= $counter++ ?></td>
                          <td><?= $row['status_code'] ?></td>
                          <td><?= $row['status_desc']  ?>
                          <td><?= $row['created_by']  ?>
                          <td><?= $row['created_date']  ?>
                          <td><?= $row['updated_by']  ?>
                          <td><?= $row['updated_date']  ?>
                          <td><button>Update</button></td>
                        </tr>
                        <?php
                      }
                    }
                    ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>No.</th>
                    <th>Kode Status</th>
                    <th>Deskripsi Status</th>
                    <th>Dibuat oleh</th>
                    <th>Dibuat tanggal</th>
                    <th>Diubah oleh</th>
                    <th>Diubah tanggal</th>
                    <th>Action</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->