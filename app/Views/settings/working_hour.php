<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Pengaturan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('/') ?>">Home</a></li>
              <li class="breadcrumb-item active">Pengaturan Jam Operasional</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <?php if (!empty(session()->getFlashdata('error'))) : ?>
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                  <?php echo session()->getFlashdata('error'); ?>
              </div>
              <?php endif; ?>
              <?php if (!empty(session()->getFlashdata('info'))) : ?>
              <div class="alert alert-info alert-dismissible fade show" role="alert">
                  <?php echo session()->getFlashdata('info'); ?>
              </div>
            <?php endif; ?>
            <div class="card card-secondary">
                <div class="card-header">
                  <h3 class="card-title">Pengaturan Jam Operasional</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="<?= base_url('/working_setting') ?>">
                  <div class="card-body">
                    <div class="form-group row">
                      <label for="jamOperasional" class="col-sm-2 col-form-label"><?= $awal['setting_name'] ?></label>
                      <div class="col-sm-4 col-md-4 col-xs-4">
                        <input type="time" class="form-control" id="jamOperasionalAwal" name="jamOperasionalAwal" placeholder="<?= $awal['setting_name'] ?>" value="<?= $awal['setting_value'] ?>">
                        <input type="hidden" id="hiddenSettingId" name="hiddenSettingId" value="1" />
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="jamOperasional" class="col-sm-2 col-form-label"><?= $akhir['setting_name'] ?></label>
                      <div class="col-sm-4 col-md-4 col-xs-4">
                        <input type="time" class="form-control" id="jamOperasionalAkhir" name="jamOperasionalAkhir" placeholder="<?= $akhir['setting_name'] ?>" value="<?= $akhir['setting_value'] ?>">
                        <input type="hidden" id="hiddenSettingId_2" name="hiddenSettingId_2" value="2" />
                      </div>
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-secondary">Submit</button>
                    <button type="reset" class="btn btn-default float-right">Batal</button>
                  </div>
                  <!-- /.card-footer -->
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->