<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>KPI</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('/') ?>">Home</a></li>
              <li class="breadcrumb-item active">KPI</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
            <div class="col-12">
              <?php if (!empty(session()->getFlashdata('error'))) : ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('error'); ?>
                </div>
                <?php endif; ?>
                <?php if (!empty(session()->getFlashdata('info'))) : ?>
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                    <?php echo session()->getFlashdata('info'); ?>
                </div>
              <?php endif; ?>
              <div class="card card-secondary">
                <div class="card-header">
                  <h3 class="card-title"><?= ($action == 'add') ? 'Tambah KPI' : 'Edit KPI' ?></h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="<?= ($action == 'add') ? base_url('/add_kpi') : base_url('/update_kpi') ?>">
                  <div class="card-body">
                    <div class="form-group row">
                      <label for="inputKategori1" class="col-sm-2 col-form-label">KPI</label>
                      <div class="col-sm-10">
                        <input type="text" class="form-control" id="kpiName" name="kpiName" placeholder="KPI" value="<?= ($action == 'add') ? '' : $kpi_detail['kpi_name'] ?>" <?= ($addFlag == true)? 'disabled':'' ?>>
                        <?php if($action == 'edit') { ?><input type="hidden" id="hiddenKpiId" name="hiddenKpiId" value="<?= $kpi_detail['kpi_id'] ?>" /><?php } ?>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <?php 
                  if(!$addFlag)
                  {
                    ?>
                    <div class="card-footer">
                      <button type="submit" class="btn btn-secondary">Submit</button>
                      <button type="reset" class="btn btn-default float-right">Batal</button>
                    </div>
                    <?php
                  }
                  ?>
                  <!-- /.card-footer -->
                </form>
              </div>
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">List KPI</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="pengaturan-kpi" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>No.</th>
                      <th>KPI</th>
                      <th>Dibuat oleh</th>
                      <th>Dibuat tanggal
                      <th>Diubah oleh</th>
                      <th>Diubah tanggal</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php 
                      if(!is_null($list_kpi))
                      {
                        $counter = 1;
                        foreach($list_kpi as $row)
                        {
                          ?>
                          <tr>
                            <td><?= $counter++ ?></td>
                            <td><?= $row['kpi_name'] ?></td>
                            <td><?= $row['created_by']  ?>
                            <td><?= $row['created_date']  ?>
                            <td><?= $row['updated_by']  ?>
                            <td><?= $row['updated_date']  ?>
                            <td><a href="<?= base_url('/edit_kpi/'.$row['kpi_id']) ?>" class="btn btn-secondary" role="button">Update</a></td>
                          </tr>
                          <?php
                        }
                      }
                      ?>
                    </tbody>
                    <tfoot>
                    <tr>
                      <th>No.</th>
                      <th>KPI</th>
                      <th>Dibuat oleh</th>
                      <th>Dibuat tanggal
                      <th>Diubah oleh</th>
                      <th>Diubah tanggal</th>
                      <th>Action</th>
                    </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
        <!-- /.container-fluid -->
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->