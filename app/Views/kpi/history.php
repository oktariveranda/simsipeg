<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>KPI</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url('/') ?>">Home</a></li>
              <li class="breadcrumb-item active">Riwayat KPI</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- /.row -->
        <div class="row">
            <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Riwayat KPI</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="riwayat-kpi" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No.</th>
                    <th>Periode</th>
                    <th>Nama Pegawai</th>
                    <th>Skor KPI</th>
                  </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>2017</td>
                      <td>Pegawai A</td>
                      <td>9.0</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>2017</td>
                      <td>Pegawai A</td>
                      <td>9.0</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>2017</td>
                      <td>Pegawai A</td>
                      <td>9.0</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>2017</td>
                      <td>Pegawai A</td>
                      <td>9.0</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>2017</td>
                      <td>Pegawai A</td>
                      <td>9.0</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>2017</td>
                      <td>Pegawai A</td>
                      <td>9.0</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>2017</td>
                      <td>Pegawai A</td>
                      <td>9.0</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>2017</td>
                      <td>Pegawai A</td>
                      <td>9.0</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>2017</td>
                      <td>Pegawai A</td>
                      <td>9.0</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>2017</td>
                      <td>Pegawai A</td>
                      <td>9.0</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>2017</td>
                      <td>Pegawai A</td>
                      <td>9.0</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>2017</td>
                      <td>Pegawai A</td>
                      <td>9.0</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>2017</td>
                      <td>Pegawai A</td>
                      <td>9.0</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>2017</td>
                      <td>Pegawai A</td>
                      <td>9.0</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>2017</td>
                      <td>Pegawai A</td>
                      <td>9.0</td>
                    </tr>
                    <tr>
                      <td>1</td>
                      <td>2017</td>
                      <td>Pegawai A</td>
                      <td>9.0</td>
                    </tr>
                  </tbody>
                  <tfoot>
                  <tr>
                  <th>No.</th>
                    <th>Periode</th>
                    <th>Nama Pegawai</th>
                    <th>Skor KPI</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            </div>    
        </div>
        <!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->